/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package library.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Scanner;
import library.entities.Book;
import library.entities.IBook;
import library.entities.Calendar;
import library.entities.ICalendar;
import library.entities.ILibrary;
import static library.entities.ILibrary.LOAN_PERIOD;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Patron;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LibraryFileHelper;
import library.returnbook.IReturnBookControl;
import library.returnbook.IReturnBookUI;
import library.returnbook.ReturnBookControl;
import library.returnbook.ReturnBookUI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author parka
 */
@RunWith(MockitoJUnitRunner.class)
public class LibraryTest {
    
    private static Scanner scannerInput;
   // private static ILibrary library;
    private static String menuString;
    private static ICalendar calendar;
    private static SimpleDateFormat dateFormat;
    private static LibraryFileHelper libraryHelper;
    private static CalendarFileHelper calendarHelper;
    private IReturnBookUI returnBookUI;
    Map<Integer, IBook> catalog;
    Map<Integer, IPatron> patrons;
    Map<Integer, ILoan> loans;
    Map<Integer, ILoan> currentLoans;
    Map<Integer, IBook> damagedBooks;

    IBookHelper bookHelper;
    IPatronHelper patronHelper;
    ILoanHelper loanHelper;
    
    public LibraryTest() {
    }
    
    @Test 
    public void testDischargeLoan(){
        String title = "t";
        String author = "a";
        String callNo = "123";
        int id = 1;
        
        String lname = "l";
        String fname = "f";
        String email = "t@t.com";
        long phoneNum = 345565L;
        int id2 = 2;
        
        ICalendar cal = Calendar.getInstance();
        Date dueDate = cal.getDate();
        cal.incrementDate(20);
        Date currDate = cal.getDate();
        int id3 = 3;
        
        Library library = new Library(bookHelper, patronHelper, loanHelper);
        boolean isDamaged = false;
       
        //act
        IPatron p = new Patron(lname, fname, email, phoneNum, id);
        Book book = new Book(author, title, callNo, id2);
        
        ILoan loan = new Loan(book, p);
        loan.commit(3, dueDate);
        loan.updateOverDueStatus(currDate);
        p.incurFine(20);
        
        System.out.println("Patron currently owes: " + p.getFinesPayable());
        
        library.dischargeLoan(loan, isDamaged);
        
        //assert
        System.out.println("Current date is: " + currDate);
        System.out.println("Due date was: " + dueDate);
        
        assertEquals(20.0, p.getFinesPayable(), .001);
    }
    
    
    /*
    @Test
    public void testCalculateOverDueFine() {
        
        //arrange
        Library library = new Library(bookHelper, patronHelper, loanHelper);
        
        ICalendar cal = Calendar.getInstance();
        Date dueDate = cal.getDate();
        cal.incrementDate(1);
        Date currDate = cal.getDate();

        System.out.println("Due date is: " + dueDate);
        System.out.println("Current date is: " + currDate);
        
        double fine;
       
        //act      
        Mockito.when(mockLoan.isOverDue()).thenReturn(true);
        Mockito.when(mockLoan.getDueDate()).thenReturn(dueDate);

        fine = library.calculateOverDueFine(mockLoan);
   
        //assert
        assertEquals(1.0, fine, 0.0001);
        
    }
*/
    
}
