/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package library.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Scanner;
import library.entities.Book;
import library.entities.IBook;
import library.entities.Calendar;
import library.entities.ICalendar;
import library.entities.ILibrary;
import static library.entities.ILibrary.LOAN_PERIOD;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Patron;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LibraryFileHelper;
import library.returnbook.IReturnBookControl;
import library.returnbook.IReturnBookUI;
import library.returnbook.ReturnBookControl;
import library.returnbook.ReturnBookUI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author parka
 */
public class CalendarTest {
    
    
    
    public CalendarTest() {
    }

    @Test
    public void testGetDaysDifference() {
        
        //arrange
        ICalendar cal = Calendar.getInstance();
        Date dueDate = cal.getDate();
        
        System.out.println(dueDate);
        
        
        //act
        cal.incrementDate(1);
        System.out.println(cal.getDate());
        long daysDifference = cal.getDaysDifference(dueDate);
        
        //assert
        assertEquals(1, daysDifference);
    }
    
}
