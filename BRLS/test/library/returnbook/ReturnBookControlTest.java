/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package library.returnbook;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Scanner;
import library.entities.Book;
import library.entities.IBook;
import library.entities.ICalendar;
import library.entities.ILibrary;
import static library.entities.ILibrary.LOAN_PERIOD;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Patron;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LibraryFileHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author parka
 */
@RunWith(MockitoJUnitRunner.class)
public class ReturnBookControlTest {
    private static Scanner scannerInput;
   // private static ILibrary library;
    private static String menuString;
    private static ICalendar calendar;
    private static SimpleDateFormat dateFormat;
    private static LibraryFileHelper libraryHelper;
    private static CalendarFileHelper calendarHelper;
    private IReturnBookUI returnBookUI;
    Map<Integer, IBook> catalog;
    Map<Integer, IPatron> patrons;
    Map<Integer, ILoan> loans;
    Map<Integer, ILoan> currentLoans;
    Map<Integer, IBook> damagedBooks;

    IBookHelper bookHelper;
    IPatronHelper patronHelper;
    ILoanHelper loanHelper;
    
    @Mock Library mockLibrary;
    
    public ReturnBookControlTest() {
    }
    
    @Before
    public void setUp() {
        
    }

    @Test
    public void testBookScanned() {
        
        //arrange
        String title = "t";
        String author = "a";
        String callNo = "123";
        int id = 1;
        
        String lname = "l";
        String fname = "f";
        String email = "t@t.com";
        long phoneNum = 345565L;
        int id2 = 2;
        
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date dueDate = c.getTime();
        c.add(Calendar.DATE, 1);
        Date currDate = c.getTime();
        int id3 = 3;
        
        calendarHelper = new CalendarFileHelper();
        calendar = calendarHelper.loadCalendar();
       
        //act
        IPatron p = new Patron(lname, fname, email, phoneNum, id);
        IReturnBookControl rBookControl = new ReturnBookControl(mockLibrary);
        returnBookUI = new ReturnBookUI(rBookControl);
        Book book = new Book(author, title, callNo, id2);
        
         
        ILoan loan = new Loan(book, p);
        loan.commit(3, dueDate);
        loan.updateOverDueStatus(currDate);
        Mockito.when(mockLibrary.getCurrentLoanByBookId(id2)).thenReturn(loan);
        Mockito.when(mockLibrary.getBookById(id2)).thenReturn(book);
        System.out.println("here");
        rBookControl.bookScanned(id2);
        
        //assert
        //assertEquals(true, loan.isOverDue());
        verify(mockLibrary).calculateOverDueFine(loan);

    }

}
